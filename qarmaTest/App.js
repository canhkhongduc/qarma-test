import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, FlatList } from 'react-native';
import BoxItem from './components/BoxItem';
import * as apiRequest from './util/apiRequest';
import * as endpoints from './constants/endpoints';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      sample: 0,
      piecesPerBox: 0,
      randomBoxes: []
    };
    this.handleButtonPress = this.handleButtonPress.bind(this);
  }
  handleButtonPress = async(e) => {
    e.preventDefault();
    const {total, sample, piecesPerBox} = this.state;
    const reqBody = {
      total: total,
      sample: sample,
      piecesPerBox: piecesPerBox
    }
    const response = await apiRequest.requestWithAxios(endpoints.checkRandomBoxes, 'POST', reqBody);
    this.setState({randomBoxes: response});
  }
  render() {
    const {randomBoxes} = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Let's start quality checking!</Text>
        <TextInput
            style={styles.textInput}
            label= 'Total'
            placeholder="Input total quantity"
            keyboardType='numeric'
            onChangeText={(total) => this.setState({total: total})}
        />
        <TextInput
            style={styles.textInput}
            label= 'Sample'
            placeholder="Input sample quantity"
            keyboardType='numeric'
            onChangeText={(sample) => this.setState({sample: sample})}
        />
        <TextInput
            style={styles.textInput}
            label= 'Pieces per box'
            placeholder="Input pieces per box"
            keyboardType='numeric'
            onChangeText={(piecesPerBox) => this.setState({piecesPerBox: piecesPerBox})}
        />
        <TouchableOpacity 
           style={styles.button}
           onPress={this.handleButtonPress}>
          <Text style={styles.text}> Check random boxes </Text>
        </TouchableOpacity>
        <FlatList 
          style={styles.flatList}
          numColumns={5}
          data={randomBoxes}
          renderItem={
            ({item}) => <BoxItem title={item.key} />
          }
        >
        </FlatList>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    top: 30,
  },
  text: {
    fontSize: 20,
  },
  textInput: {
    height: 50,
    width: 250,
    fontSize: 20,
    top: 30,
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 15,
    textAlign: 'center',
    marginBottom: 20,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#33ccff',
    padding: 10,
    borderRadius: 15,
    top: 30,
    width: 250
  },
  flatList: {
    top: 50,
    flexDirection: 'column'
  }
});
