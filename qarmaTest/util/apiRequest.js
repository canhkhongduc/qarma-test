const axios = require('axios');

export async function requestWithAxios(api, method, requestBody) {
    const option = {
        url: api,
        method: method
    };

    if (requestBody) 
        option.data = requestBody;
    
    const response = await axios(option);

    if (response.status === 200 || response.status === 201) 
        return response.data;
    
    throw new Error(response.statusText);
}