import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
export default function Item(props) {
    return (
        <View style={styles.item}>
            <Text style={styles.text}>{props.title}</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    item:{
        alignItems: 'center',
        backgroundColor: '#f9c2ff',
        padding: 5,
        width: 50,
        height: 45,
        right: 5
    },
    text: {
      fontSize: 20,
    },
});
  
