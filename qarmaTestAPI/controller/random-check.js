module.exports.randomCheck = async function (req) {
  let [randomBoxes, totalNumberOfBoxes, numberOfBoxesToCheck] = [[], 0, 0];
  const {total, sample, piecesPerBox} = req.body;
  totalNumberOfBoxes = Math.ceil(total / piecesPerBox);
  numberOfBoxesToCheck = Math.ceil(sample / piecesPerBox);
  while(randomBoxes.length < numberOfBoxesToCheck) {
    let randomBox = {key: Math.floor(Math.random() * totalNumberOfBoxes) + 1};
    if(randomBoxes.indexOf(randomBox) === -1) randomBoxes.push(randomBox);
  }
  return randomBoxes;
};
