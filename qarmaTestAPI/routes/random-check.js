const express = require('express');
const router = express.Router();
const controller = require('../controller/random-check');

router.post('/', async (req, res) => {
	let randomBoxes = await controller.randomCheck(req);
  return res.json(randomBoxes);
});

module.exports = router;
